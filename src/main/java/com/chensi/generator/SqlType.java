package com.chensi.generator;

/**
 * sql语句的种类
 * @author chensi
 * @version 2017-7-20 下午5:37:48
 */
public enum SqlType {

	list,listByEntity,countByEntity,get,getByEntity,save,update,del,delByEntity,delList;
	
}
